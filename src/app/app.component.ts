import { Component } from '@angular/core';
import { NotificationService } from './arch/notification/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Micro Frontends Code Club';

  constructor(private notificationService: NotificationService) {}

  showNotification() {
    this.notificationService.notify(
      'Hello',
      'This is a notification from the architecture module',
      'success'
    );
  }
}
