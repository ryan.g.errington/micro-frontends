// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// App
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Custom
import { UiModule } from './ui/ui.module';
import { ArchModule } from './arch/arch.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, UiModule, ArchModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
