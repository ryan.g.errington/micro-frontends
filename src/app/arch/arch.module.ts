// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Custom
import { NotificationService } from './notification/notification.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [NotificationService]
})
export class ArchModule {}
