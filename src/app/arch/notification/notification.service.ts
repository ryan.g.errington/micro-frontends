import { Injectable } from '@angular/core';
import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;

@Injectable()
export class NotificationService {
  constructor() {}

  notify(title: string, message: string, type: 'success' | 'error') {
    swal(title, message, type);
  }
}
