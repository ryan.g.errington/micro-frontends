import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { HeroImageComponent } from './hero-image/hero-image.component';
import { TitleComponent } from './title/title.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ButtonComponent, HeroImageComponent, TitleComponent],
  exports: [ButtonComponent, HeroImageComponent, TitleComponent]
})
export class UiModule {}
